# Better commands
alias ll='ls -gh --group-directories-first'
alias la='ls -ghA --group-directories-first'
alias tree='tree -C'
alias lsblk="lsblk --output NAME,MOUNTPOINT,FSTYPE,LABEL,FSSIZE,FSUSED,LABEL,UUID,MODEL,VENDOR"
alias -- -='cd -'

# Shortcuts
alias fuck='sudo $(history -p \!\!)'
alias what='$(history -p \!\!) | bat'
alias vimrc='vim ~/.vim/vimrc'
alias vimi3="vim $HOME/.config/i3/config"
alias workspaces="i3-msg -t get_workspaces"
alias fixsnaps='apparmor_parser -r /var/lib/snapd/apparmor/profiles/* && usysconf run apparmor -f'
alias reload-shell="exec $SHELL -l"
alias reload-profile="source ${HOME}/.bashrc"
alias temp="nvidia-settings -q GPUCoreTemp | grep -m1 GPU | perl -ne 'print \\\$1 if /: (\\d+)./'"

export drive="$HOME/gdrive/stuff"

# BECAUSE SOMETIMES I GET STUCK IN CAPS LOCK
alias MODKEYS="$HOME/.scripts/modkeys"
alias chcaps="xcape -e 'Hyper_L=Control_L|Alt_L|comma'"

alias starwars='telnet towel.blinkenlights.nl'

