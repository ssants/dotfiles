#!/bin/bash
# From bluz71 <bluz71.github.io/2018/11/26/fuzzy-finding-in-bash-with-fzf.html>

ffe() {
	local file=$(
		fzf --no-multi --preview 'bat --color=always --line-range :500 {}'
	)
	if [[ -n $file ]]; then
		$EDITOR $file
	fi
}

fge() {
	if [[ $# == 0 ]]; then
		echo 'Error: search term was not provided'
		return
	fi
	local match=$(
		rg --color=never --line-number "$1" |
				fzf --no-multi --delimiter :\
						--preview "bat --color=always --line-range {2}: {1}"
		)
	local file=$(echo "$match" | cut -d':' -f1)
	if [[ -n $file ]]; then
		$EDITOR $file +$(echo "$match" | cut -d':' -f2)
	fi
}

fkill() {
	local pids
	if [ "$UID" != "0" ]; then
		pids=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
	else
		pids=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
	fi
	if [ "x$pids" != "x" ]; then
		echo $pids | xargs kill -${1:-9}
	fi
}
