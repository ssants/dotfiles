#!/usr/bin/python3
# Lemonpi3.py (Get it? pie? py? no?)
# Subscribes to i3 ipc, output read by ./lemonade
import sys
import i3ipc
import subprocess

i3 = i3ipc.Connection()
DP = str(sys.argv[1])

# Colors for lemonbar
BG="#9f000000"
FOCUS="#ca444444"
BG2="#63000000"
FOCUS2="#ca000000"
URG="#a00"
INVIS="#00ffffff"

# For minimizing/hiding lemonbar
# 0=normal, 1=timeonly, 2=nobar
bar = 0

###############################
# Workspace Events
###############################
# Strings to format for different types of workspaces
WS_FMT_FOCUS = "%{{B{color}}} {s} %{{B-}}"
WS_FMT_URG   = "%{{A:i3-msg -q workspace {s}:}}%{{B{color}}} {s} %{{A}}%{{B-}}"
WS_FMT_OTHER = "%{{A:i3-msg -q workspace {s}:}}%{{B{color}}} {s} %{{A}}%{{B-}}"

def on_workspace(i3, _):
    spaces = filter(lambda ws: ws.output == DP, i3.get_workspaces())
    ws_str = "Workspace "
    if bar == 2:
        return
    for s in sorted(spaces, key=lambda s: s.name):
        if s.focused:
            ws_str += WS_FMT_FOCUS.format(color=FOCUS if bar == 0 else FOCUS2, s=s.name)
        elif s.urgent:
            ws_str += WS_FMT_URG.format(color=URG, s=s.name)
        else:
            ws_str += WS_FMT_OTHER.format(color=BG if bar == 0 else BG2, s=s.name)
    print(ws_str, flush=True)

# Explicit on_workspace_focus to update the title
#   when switching to an empty one (on_window not called)
def on_workspace_focus(i3, w):
    if not w or not w.current.window:
        on_window(i3, None)
    on_workspace(i3, None)

###############################
# Window Events
###############################
# Note: Sometimes other workspaces can focus depending on the program running there
#       e.g.: When spotify in Firefox switches songs, it updates the title,
#             but doesn't switch back

# Custom titles (i.e. icons) for certain programs
titles = dict()
# gnome-terminal: remove initial "ssants@solus:"
titles['Gnome-terminal'] = lambda t: "\\uf120 " + t[14:] 
# Firefox: remove trailing " - Mozilla Firefox" and cut off after 100 chars
titles['Firefox'] = lambda t: "\\uf269 " + t[:-18] if len(t[:-18]) < 100 else t[:100]+'...' 
# Android Messages
titles['Android Messages'] = lambda t: "\\uf086 " + t
# LibreOffice Writer: remove trailing   " - LibreOffice Writer"
titles['libreoffice-writer'] = lambda t: "\\uf1c2 " + t[:-21]

def on_window(i3, w):
    win_str = "Title "
    if not w or bar > 0: 
        print(win_str, flush=True)
        return
    title = w.container.name
    cls = w.container.window_class
    if cls in titles:
        win_str += titles[cls](title)
    else:
        win_str += title
    print(win_str, flush=True)

def on_window_close(i3, w):
    f = i3.get_tree().find_focused()
    if not f.window:
        print('Title ', flush=True)
    else:
        on_window(i3, w)

###############################
# Binding Events
###############################
def on_binding(i3, e):
    b = e.binding
    if b.symbol == 'F11': 
        fullscreen(b)
    elif b.symbol in ['XF86AudioLowerVolume', 'XF86AudioRaiseVolume', 'XF86AudioMute']: 
        # see ./volume
        pass
    else:
        return

def fullscreen(b):
    global bar
    if 'shift' in b.mods:
        bar = 2 if bar != 2 else 0
    else:
        bar = 1 if bar != 1 else 0
    print('Full', bar, flush=True)
    on_workspace(i3, None)
    on_window(i3, None)

###############################
# Main
###############################
i3.on('workspace::init',  on_workspace)
i3.on('workspace::focus', on_workspace_focus)
i3.on('workspace::empty', on_workspace)
i3.on('window::new',   on_window)
i3.on('window::focus', on_window)
i3.on('window::title', on_window)
i3.on('window::close', on_window_close)
i3.on('binding::run', on_binding)

# Call first to ensure they show up on first screen
on_workspace_focus(i3, None)

i3.main()

