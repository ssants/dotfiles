# check ~/.scripts/ for other commonly used stuff

### Variables ###
# Mod3 <-- Hyper_L <-- Caps_Lock, see ~/.scripts/modkeys
set $mod Mod3
set $up k
set $down j
set $left h
set $right l
set $dp0 "DP-4"
set $dp1 "DP-2"

### Define other stuff ###
for_window [class="^.*"] border pixel 1 
# Floating windows 
for_window [window_role="pop-up"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [title="Preferences"] floating enable
for_window [title="Settings"] floating enable
for_window [title="Steam - News"] floating enable
for_window [title="Network Connections"] floating enable
for_window [class="Android Messages"] resize set 500 750, move scratchpad

# Inner gaps to match with lemonbar gaps
gaps inner 9
gaps outer 0

font pango:Clear Sans 10

### Autolaunch ###
exec --no-startup-id albert
# Network manager
exec --no-startup-id nm-applet
exec --no-startup-id ~/.scripts/modkeys
exec --no-startup-id feh --bg-fill ~/Pictures/StarryNight.jpg --nofehbg
exec --no-startup-id compton --config /dev/null
exec --no-startup-id conky -d -c ~/.scripts/bar/empty_conk0
exec --no-startup-id conky -d -c ~/.scripts/bar/empty_conk1
exec --no-startup-id ~/.scripts/bar/lemonade $dp0
exec --no-startup-id ~/.scripts/bar/lemonade $dp1
exec --no-startup-id unclutter --timeout 3 --jitter 100 -b
# Not necessary since for Gnome/i3
#exec --no-startup-id /usr/lib64/polkit-gnome/polkit-gnome-authentication-agent-1
#exec --no-startup-id /usr/lib64/gnome-settings-daemon/gsd-xsettings
#exec --no-startup-id compton --config ~/.config/compton/compton.conf

### Keybindings ###
# Firefox
bindsym $mod+f exec firefox 
bindsym $mod+Shift+f exec firefox --private-window

# I use albert for this stuff
#bindsym $mod+Shift+s exec i3lock -c 222222 # Lock 
#bindsym $mod+Shift+Escape exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
#bindsym $mod+Shift+Backspace exec sudo -A reboot now 

# Terminal stuff
bindsym $mod+Return exec i3-sensible-terminal
bindsym $mod+r exec --no-startup-id i3-sensible-terminal -e ranger
bindsym $mod+i exec --no-startup-id i3-sensible-terminal -e htop

bindsym $mod+Ctrl+F11 fullscreen toggle
# Kill focused window
bindsym $mod+Shift+q kill
# Change focus
bindsym $mod+$left  focus left
bindsym $mod+$down  focus down
bindsym $mod+$up    focus up
bindsym $mod+$right focus right
# Move focused window
bindsym $mod+Shift+$left  move left
bindsym $mod+Shift+$down  move down
bindsym $mod+Shift+$up    move up
bindsym $mod+Shift+$right move right
# Resize windows 
bindsym $mod+Shift+y	exec --no-startup-id ~/.scripts/i3resize left
bindsym $mod+Shift+u	exec --no-startup-id ~/.scripts/i3resize down
bindsym $mod+Shift+i	exec --no-startup-id ~/.scripts/i3resize up
bindsym $mod+Shift+o	exec --no-startup-id ~/.scripts/i3resize right

# Split in hori/verti orientation
# Maybe change later, mod+shift+v is awkward
bindsym $mod+Shift+v split h
bindsym $mod+v split v
# Change container layout (stacked, tabbed, toggle split)
bindsym $mod+e layout toggle split
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
# Toggle tiling / floating
bindsym $mod+Shift+space floating toggle
# Toggle sticky /floating
bindsym $mod+F12 sticky toggle floating toggle
# Change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# Focus the parent container
bindsym $mod+a focus parent
# Focus the child container
bindsym $mod+d focus child 

# Mouse options
focus_follows_mouse no
mouse_warping none

# Move current window to scratchpad
bindsym $mod+Shift+minus move scratchpad
# Show the next scrachpad window or hide the focused scratchpad window
# If there are multiple scratchpad windows, this command cycles through them
bindsym $mod+minus scratchpad show
 
# Media Buttons
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioPause exec playerctl pause
bindsym XF86AudioPrev exec playerctl previous
bindsym XF86AudioNext exec playerctl next
# Volume stuff, also see ~/.scripts/bar/lemonpi3.py: on_binding
# Raise/lower volume
#bindsym XF86AudioRaiseVolume exec --no-startup-id amixer -D pulse sset Master 5%+ && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
#bindsym XF86AudioLowerVolume exec --no-startup-id amixer -D pulse sset Master 5%- && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
bindsym XF86AudioRaiseVolume exec --no-startup-id ~/.scripts/bar/volume up
bindsym XF86AudioLowerVolume exec --no-startup-id ~/.scripts/bar/volume down
# Cycle through audio sinks
bindsym XF86AudioMute exec --no-startup-id ~/.scripts/audioswitch
#bindsym XF86AudioMute (above with) toggle-mute # Toggle muting 

# Other
bindsym Print exec scrot -q 100
bindsym XF86Sleep exec i3lock-next
# See ~/.scripts/bar/lemonpi3.py: on_binding for below
bindsym $mod+F11 nop
bindsym $mod+Shift+F11 nop

### Worspaces ###
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $wsf1 "F1"
set $wsf2 "F2"
set $wsf3 "F3"
set $wsf4 "F4"
set $wsf5 "F5"

# Assign workspaces for two monitors
# check monitors using `xrandr`
workspace $ws1 output $dp0
workspace $ws2 output $dp0
workspace $ws3 output $dp0
workspace $ws4 output $dp0
workspace $ws5 output $dp0
workspace $wsf1 output $dp1
workspace $wsf2 output $dp1
workspace $wsf3 output $dp1
workspace $wsf4 output $dp1
workspace $wsf5 output $dp1

# Assign workspaces (one monitor)
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+F1 workspace $wsf1
bindsym $mod+F2 workspace $wsf2
bindsym $mod+F3 workspace $wsf3
bindsym $mod+F4 workspace $wsf4
bindsym $mod+F5 workspace $wsf5

# Move windows
bindsym $mod+Shift+1 move workspace $ws1
bindsym $mod+Shift+2 move workspace $ws2
bindsym $mod+Shift+3 move workspace $ws3
bindsym $mod+Shift+4 move workspace $ws4
bindsym $mod+Shift+5 move workspace $ws5
bindsym $mod+Shift+F1 move workspace $wsf1
bindsym $mod+Shift+F2 move workspace $wsf2
bindsym $mod+Shift+F3 move workspace $wsf3
bindsym $mod+Shift+F4 move workspace $wsf4
bindsym $mod+Shift+F5 move workspace $wsf5

### Colors ###
# TODO: use colors better
set $bg_c    #404552
set $focus_c #3685e2
set $activ_c #5294e2
set $inact_c #666666
set $urg_c   #ff5757
set $blank_c #fafafa
set $e_c     #eeeeee
set $f_c     #ffffff
set $five_c  #555757

client.focused		$bg_c    $bg_c $blank_c $urg_c   $blank_c
client.focused_inactive $inact_c $bg_c $e_c     $inact_c $inact_c
client.unfocused	$bg_c    $bg_c $e_c     $urg_c   $bg_c
client.urgent		$urg_c   $bg_c $f_c     $five_c  $bg_c
client.background	$bg_c    $bg_c 

### i3 Management ###
# Reload the configeration file
bindsym $mod+Shift+c reload
# Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
