source /usr/share/defaults/etc/profile

[ -f ~/.bash_aliases ] && source ~/.bash_aliases

export EDITOR="vim"

export PATH="$PATH:/home/ssants/.local/bin"
export PATH="$PATH:$(du "$HOME/.scripts/" | cut -f2 | tr '\n' ':')" #custom scripts

#export PYENV_ROOT="$HOME/.pyenv"
#export PATH="$PYENV_ROOT/bin:$PATH"

export BAT_THEME="OneHalfDark"

# FZF
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
export FZF_DEFAULT_COMMAND='fd --type f --color=never'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--preview 'bat --color=always --line-range :500 {}' --layout=reverse --height 40%"
export FZF_ALT_C_COMMAND='fd --type d . --color=never'
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -100'"
[ -f ~/.scripts/fzf_functions.sh ] && source ~/.scripts/fzf_functions.sh

# "•••" with different colors
# TODO update colors based on err code
PS1=" \[\e[0;32m\]•\[\e[0;33m\]•\[\e[0;31m\]• \[\e[1;36m\]\w\[\e[0;37m\] "


# Android / Dart / Flutter
[ -f "$HOME/.scripts/flutter_completion.bash" ] && source "$HOME/.scripts/flutter_completion.bash"
export ANDROID_HOME="$HOME/Android/sdk/"
export PATH="$PATH:$HOME/Android/sdk/platform-tools"
export PATH="$PATH:$HOME/bin/flutter/bin:$HOME/bin/flutter/bin/cache/dart-sdk/bin"
export FF_CSS="$HOME/.mozilla/firefox/j33eww1o.default/chrome"
